﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;

namespace ReadAndCheckExcelFile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_show_folder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txt_folder_path.Text = fbd.SelectedPath;

                DirectoryInfo d = new DirectoryInfo(@fbd.SelectedPath);//Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.xlsx"); //Getting Text files
                loadListFile(Files);
            }
        }

        void loadListFile(FileInfo[] Files)
        {
            foreach (FileInfo file in Files)
            {
                ListViewItem item1 = new ListViewItem();
                item1.Text = file.Name;
                ListViewItem.ListViewSubItem subItem = new ListViewItem.ListViewSubItem(item1, "");
                ListViewItem.ListViewSubItem subItem1 = new ListViewItem.ListViewSubItem(item1, "");
                item1.SubItems.Add(subItem);
                item1.SubItems.Add(subItem1);
                listView1.Items.Add(item1);
            }
        }

        static int openfile(string mySheet, ListViewItem lvItem)
        {
            _Application excelApp = new _Excel.Application();

            Workbook wb;
            Worksheet ws;

            try {
                wb = excelApp.Workbooks.Open(@mySheet);
            } catch(Exception ex)
            {
                return 0;
            }
            //sheet one
            ws = wb.Worksheets[1];
            lvItem.SubItems[1].Text = "- " + ws.Name;

            //tiền Cotton
            int beginRowI = 20;
            int beginColumnI = 3;
            int endRowI = 28;
            int endColumnI = 17;
            checkRow(ws, beginRowI, beginColumnI, endRowI, endColumnI, lvItem);

            beginRowI = 20;
            beginColumnI = 4;
            endRowI = 30;
            endColumnI = 17;
            checkColumn(ws, beginRowI, beginColumnI, endRowI, endColumnI, lvItem);


            //tiền Polyme
            int beginRowII = 32;
            int beginColumnII = 3;
            int endRowII = 37;
            int endColumnII = 17;
            checkRow(ws, beginRowII, beginColumnII, endRowII, endColumnII, lvItem);

            beginRowII = 32;
            beginColumnII = 4;
            endRowII = 38;
            endColumnII = 17;
            checkColumn(ws, beginRowII, beginColumnII, endRowII, endColumnII, lvItem);

            //tiền Kim Loại
            int beginRowIII = 40;
            int beginColumnIII = 3;
            int endRowIII = 44;
            int endColumnIII = 17;
            checkRow(ws, beginRowIII, beginColumnIII, endRowIII, endColumnIII, lvItem);

            beginRowIII = 40;
            beginColumnIII = 4;
            endRowIII = 45;
            endColumnIII = 17;
            checkColumn(ws, beginRowIII, beginColumnIII, endRowIII, endColumnIII, lvItem);

            int rowIV = 46;
            int beginColumnIV = 4;
            int endColumnIV = 17;
            checkTongCong(ws, rowIV, beginColumnIV, endColumnIV, lvItem);


            //--------sheet two
            ws = wb.Worksheets[2];
            lvItem.SubItems[1].Text = lvItem.SubItems[1].Text + "\r\n\r\n- " + ws.Name;

            //tiền Cotton
            beginRowI = 20;
            beginColumnI = 3;
            endRowI = 28;
            endColumnI = 9;
            checkRow(ws, beginRowI, beginColumnI, endRowI, endColumnI, lvItem);

            beginRowI = 20;
            beginColumnI = 4;
            endRowI = 30;
            endColumnI = 9;
            checkColumn(ws, beginRowI, beginColumnI, endRowI, endColumnI, lvItem);

            //tiền Polyme
            beginRowII = 32;
            beginColumnII = 3;
            endRowII = 37;
            endColumnII = 9;
            checkRow(ws, beginRowII, beginColumnII, endRowII, endColumnII, lvItem);

            beginRowII = 32;
            beginColumnII = 4;
            endRowII = 38;
            endColumnII = 9;
            checkColumn(ws, beginRowII, beginColumnII, endRowII, endColumnII, lvItem);

            //tiền Kim Loại
            beginRowIII = 40;
            beginColumnIII = 3;
            endRowIII = 44;
            endColumnIII = 9;
            checkRow(ws, beginRowIII, beginColumnIII, endRowIII, endColumnIII, lvItem);

            beginRowIII = 40;
            beginColumnIII = 4;
            endRowIII = 45;
            endColumnIII = 9;
            checkColumn(ws, beginRowIII, beginColumnIII, endRowIII, endColumnIII, lvItem);

            rowIV = 46;
            beginColumnIV = 4;
            endColumnIV = 9;
            checkTongCong(ws, rowIV, beginColumnIV, endColumnIV, lvItem);

            wb.Close(false, Type.Missing, Type.Missing);
            excelApp.Quit();
            GC.Collect();

            return 1;
        }

        static void checkTongCong(Worksheet ws, int row, int beginColumn, int endColumn, ListViewItem lvItem)
        {
            int rowTongTienCotton = 30;
            int rowTongTienPolyme = 38;
            int rowTongTienKimloai = 45;
            for (int column = beginColumn; column <= endColumn; column++)
            {
                string tongTienCotton = getCell(ws, rowTongTienCotton, column);
                string tongTienPolyme = getCell(ws, rowTongTienPolyme, column);
                string tongTienKimLoai = getCell(ws, rowTongTienKimloai, column);

                decimal tongTienCottonNumber = 0;
                decimal tongTienPolymeNumber = 0;
                decimal tongTienKimLoaiNumber = 0;

                decimal sumTongTien = 0;

                if (tongTienCotton != "")
                {
                    tongTienCotton = tongTienCotton.Replace(",", ".");
                    tongTienCottonNumber = Convert.ToDecimal(tongTienCotton);
                }
                if (tongTienPolyme != "")
                {
                    tongTienPolyme = tongTienPolyme.Replace(",", ".");
                    tongTienPolymeNumber = Convert.ToDecimal(tongTienPolyme);
                }
                if (tongTienKimLoai != "")
                {
                    tongTienPolyme = tongTienPolyme.Replace(",", ".");
                    tongTienKimLoaiNumber = Convert.ToDecimal(tongTienPolyme);
                }

                sumTongTien = tongTienCottonNumber + tongTienPolymeNumber + tongTienKimLoaiNumber;

                string tongTienColumn = getCell(ws, row, column);
                decimal tongTienColumnNumber = 0;

                if (tongTienColumn != "")
                {
                    tongTienColumn = tongTienColumn.Replace(",", ".");
                    tongTienColumnNumber = Convert.ToDecimal(tongTienColumn);

                    if(sumTongTien != tongTienColumnNumber)
                    {
                        string rowTitle = getCell(ws, row, MyGlobals.titleHeaderColumn);
                        string columnTitle = getColumnTitle(ws, MyGlobals.titleHeaderRow, column);
                        lvItem.SubItems[1].Text = lvItem.SubItems[1].Text + "\r\n- Nhập sai ở dòng: " + rowTitle + " , cột: " + columnTitle + " , số tiền sai: " + tongTienColumnNumber + " , số tiền đúng: " + sumTongTien;
                        //MessageBox.Show("Bạn đã nhập sai ở dòng: " + rowTitle + " , cột: " + columnTitle + " , số tiền sai: " + tongTienColumnNumber + " , số tiền đúng: " + sumTongTien);
                    }
                }
            }
        }

        static void checkRow(Worksheet ws, int beginRow, int beginColumn, int endRow, int endColumn, ListViewItem lvItem)
        {
            int columnTongQuyDauKy = 4;
            int columnCongThu = 9;
            int columnCongChi = 14;
            int columnTongQuyCuoiKy = 15;

            int rowTongCong = 46;
            int columnThuTuTCTDKhac = 6;
            int columnThuTuKhachHang = 7;
            int columnThuNoiBoTCTD = 8;
            int columnTiLeThu = 16;

            int columnChiChoTCTDKhac = 11;
            int columnChiChoKhachHang = 12;
            int columnChiNoiBoTCTD = 13;
            int columnTiLeChi = 17;

            for (int row = beginRow; row <= endRow; row++)
            {
                int countRow = 0;
                string loaiTien = "";
                string tienMat = "";
                string tienMatColumnTongCong = "";

                decimal sumTongTienCheckTongQuyCuoiKy = 0;
                decimal sumTongTienTiLeThu = 0;
                decimal sumTongTienTiLeThuColumnTongCong = 0;

                decimal sumTongTienTiLeChi = 0;
                decimal sumTongTienTiLeChiColumnTongCong = 0;

                for (int column = beginColumn; column <= endColumn; column++)
                {
                    if (countRow == 0)
                    {
                        if (ws.Cells[row, column].Value2 != null)
                        {
                            loaiTien = ws.Cells[row, column].Value2;
                            loaiTien = loaiTien.Trim();
                            loaiTien = loaiTien.Replace("đ", string.Empty);
                            loaiTien = loaiTien.Replace(".", string.Empty);
                        }
                        countRow = 1;
                        continue;
                    }

                    if (loaiTien != "")
                    {
                        if (ws.Cells[row, column].Value2 != null)
                        {
                            tienMat = ws.Cells[row, column].Value2;
                            tienMat = tienMat.Replace(",", ".");
                        }
                        else
                        {
                            tienMat = "";
                        }
                    }

                    decimal tienMatNumber = 0;
                    if (tienMat != "")
                    {
                        tienMatNumber = Convert.ToDecimal(tienMat) * MyGlobals.donViTinh;
                    }

                    //---begin ti le thu
                    if (column == columnThuTuTCTDKhac ||
                        column == columnThuTuKhachHang ||
                        column == columnThuNoiBoTCTD)
                    {
                        sumTongTienTiLeThu += tienMatNumber;
                        tienMatColumnTongCong = getCell(ws, rowTongCong, column);
                        if (tienMatColumnTongCong != "")
                        {
                            tienMatColumnTongCong = tienMatColumnTongCong.Replace(",", ".");

                            decimal tienMatNumberColumnTongCong = Convert.ToDecimal(tienMatColumnTongCong) * MyGlobals.donViTinh;
                            sumTongTienTiLeThuColumnTongCong += tienMatNumberColumnTongCong;
                        }
                    }
                    if (column == columnTiLeThu)
                    {
                        decimal calculateTongTienTiLeThu = (sumTongTienTiLeThu) / (sumTongTienTiLeThuColumnTongCong) * 100;
                        calculateTongTienTiLeThu = decimal.Round(calculateTongTienTiLeThu, 2, MidpointRounding.AwayFromZero);

                        if (calculateTongTienTiLeThu != (tienMatNumber / MyGlobals.donViTinh))
                        {
                            string rowTitle = getCell(ws, row, MyGlobals.titleHeaderColumn);
                            string columnTitle = getColumnTitle(ws, MyGlobals.titleHeaderRow, column);
                            lvItem.SubItems[1].Text = lvItem.SubItems[1].Text + "\r\n- Nhập sai Tỉ lệ thu ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + (tienMatNumber / MyGlobals.donViTinh).ToString() + " , tiền đúng là: " + calculateTongTienTiLeThu.ToString();
                        }
                    }
                    //---end ti le thu


                    //---begin ti le chi
                    if (column == columnChiChoTCTDKhac ||
                        column == columnChiChoKhachHang ||
                        column == columnChiNoiBoTCTD)
                    {
                        sumTongTienTiLeChi += tienMatNumber;
                        tienMatColumnTongCong = getCell(ws, rowTongCong, column);
                        if (tienMatColumnTongCong != "")
                        {
                            tienMatColumnTongCong = tienMatColumnTongCong.Replace(",", ".");

                            decimal tienMatNumberColumnTongCong = Convert.ToDecimal(tienMatColumnTongCong) * MyGlobals.donViTinh;
                            sumTongTienTiLeChiColumnTongCong += tienMatNumberColumnTongCong;
                        }
                    }
                    if (column == columnTiLeChi)
                    {
                        decimal calculateTongTienTiLeChi = (sumTongTienTiLeChi) / (sumTongTienTiLeChiColumnTongCong) * 100;
                        calculateTongTienTiLeChi = decimal.Round(calculateTongTienTiLeChi, 2, MidpointRounding.AwayFromZero);

                        if (calculateTongTienTiLeChi != (tienMatNumber / MyGlobals.donViTinh))
                        {
                            string rowTitle = getCell(ws, row, MyGlobals.titleHeaderColumn);
                            string columnTitle = getColumnTitle(ws, MyGlobals.titleHeaderRow, column);
                            lvItem.SubItems[1].Text = lvItem.SubItems[1].Text + "\r\n- Nhập sai Tỉ lệ chi ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + (tienMatNumber / MyGlobals.donViTinh).ToString() + " , tiền đúng là: " + calculateTongTienTiLeChi.ToString();
                        }
                    }
                    //---end ti le chi

                    if (loaiTien != "" && tienMat != "")
                    {
                        decimal loaiTienNumber = Convert.ToDecimal(loaiTien);
                        decimal result = (tienMatNumber % loaiTienNumber);

                        //---begin tong quy cuoi ky
                        if(column == columnTongQuyDauKy || column == columnCongThu)
                        {
                            sumTongTienCheckTongQuyCuoiKy += tienMatNumber;
                        }
                        if (column == columnCongChi)
                        {
                            sumTongTienCheckTongQuyCuoiKy -= tienMatNumber;
                        }
                        
                        if (column == columnTongQuyCuoiKy)
                        {
                            if (tienMatNumber != sumTongTienCheckTongQuyCuoiKy)
                            {
                                string rowTitle = getCell(ws, row, MyGlobals.titleHeaderColumn);
                                string columnTitle = getColumnTitle(ws, MyGlobals.titleHeaderRow, column);
                                lvItem.SubItems[1].Text = lvItem.SubItems[1].Text + "\r\n- Nhập sai Tổng Quỹ Cuối kỳ (2) + (7) - (12) ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + tienMat + " , tiền đúng là: " + sumTongTienCheckTongQuyCuoiKy / 1000;
                                //MessageBox.Show("Bạn đã cộng sai Tổng Quỹ Cuối kỳ (2) + (7) - (12) ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + tienMat + " , tiền đúng là: " + sumTongTienCheckTongQuyCuoiKy/1000);
                            }
                        }
                        //---end tong quy cuoi ky

                        if (column != columnTiLeThu && 
                            column != columnTiLeChi && 
                            result > 0)
                        {
                            string rowTitle = getCell(ws, row, MyGlobals.titleHeaderColumn);
                            string columnTitle = getColumnTitle(ws, MyGlobals.titleHeaderRow, column);
                            lvItem.SubItems[1].Text = lvItem.SubItems[1].Text + "\r\n- Nhập sai ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + tienMat;
                            //MessageBox.Show("Bạn đã nhập sai ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + tienMat);
                        }
                    }
                }
            }
        }

        static void checkColumn(Worksheet ws, int beginRow, int beginColumn, int endRow, int endColumn, ListViewItem lvItem)
        {
            for (int column = beginColumn; column <= endColumn; column++) 
            {
                string tienMat = "";
                decimal sumRow = 0;

                for (int row = beginRow; row <= endRow; row++)
                {
                    if (ws.Cells[row, column].Value2 != null)
                    {
                        tienMat = ws.Cells[row, column].Value2;
                        tienMat = tienMat.Replace(",", ".");
                    }
                    else
                    {
                        tienMat = "";
                    }

                    if (row == endRow)
                    {
                        if (tienMat != "")
                        {
                            decimal tienMatNumber = Convert.ToDecimal(tienMat);
                            if(tienMatNumber != sumRow) {
                                string rowTitle = getCell(ws, row, MyGlobals.titleHeaderColumn);
                                string columnTitle = getColumnTitle(ws, MyGlobals.titleHeaderRow, column);
                                lvItem.SubItems[1].Text = lvItem.SubItems[1].Text + "\r\n- Nhập sai ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + tienMatNumber.ToString() + " số tiền đúng: " + sumRow.ToString();
                                //MessageBox.Show("Bạn đã nhập sai ở dòng: " + rowTitle + " , cột: " + columnTitle + " , tiền nhập sai: " + tienMatNumber.ToString() + " số tiền đúng: " + sumRow.ToString());
                            }
                        }
                    } else {
                        if (tienMat != "")
                        {
                            decimal tienMatNumber = Convert.ToDecimal(tienMat);
                            sumRow += tienMatNumber;
                        }
                    }
                }
            }
        }

        static string getCell(Worksheet ws, int row, int column)
        {
            if (ws.Cells[row, column].Value2 != null)
            {
                string valueCell = ws.Cells[row, column].Value2;
                return valueCell.Trim();
            } else
            {
                return "";
            }
        }

        static string getColumnTitle(Worksheet ws, int row, int column)
        {
            if (ws.Cells[row, column].Value2 != null)
            {
                string valueCell = ws.Cells[row, column].Value2;
                return valueCell.Trim();
            }
            else
            {
                row = row - 1;
                if (ws.Cells[row, column].Value2 != null)
                {
                    string valueCell = ws.Cells[row, column].Value2;
                    return valueCell.Trim();
                }
                else
                {
                    return "";
                }
            }
        }

        private void kiem_tra_file_Click(object sender, EventArgs e)
        {
            //TO DO check empty folder
            string folderLogFile = txt_folder_path.Text + "\\logfiles\\";

            string folderCheckedFile = txt_folder_path.Text + "\\file_da_kiem_tra\\";

            if (!Directory.Exists(folderLogFile))
            {
                Directory.CreateDirectory(folderLogFile);
            }

            if (!Directory.Exists(folderCheckedFile))
            {
                Directory.CreateDirectory(folderCheckedFile);
            }

            foreach (ListViewItem item in this.listView1.CheckedItems)
            {
                //var tag = item.Tag;
                string filePath = txt_folder_path.Text + "\\" + item.Text;
                item.SubItems[2].Text = "Đang kiểm tra";
                int resultOpenFile = openfile(filePath, item);
                item.SubItems[2].Text = "Kiểm tra xong";

                if(resultOpenFile == 1) { //file tồn tại
                    string textLog = item.SubItems[1].Text;
                    string statusFileTxt = "_thanh_cong";
                    if(textLog.Contains("sai") == true)
                    {
                        statusFileTxt = "_loi";
                    }
                    string logFile = item.Text;
                    string[] logFileSplit = logFile.Split('.');
                    logFile = logFileSplit[0];
                    string logFilePath = folderLogFile + logFile + statusFileTxt + ".log";

                    FileStream fs = new FileStream(logFilePath, FileMode.OpenOrCreate);
                    StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
                    sw.WriteLine(item.SubItems[1].Text);
                    sw.Close();
                    fs.Close();

                    string sourceFile = @filePath;
                    string filePathMove = folderCheckedFile + "\\" + item.Text;
                    string destinationFile = @filePathMove;

                    // To move a file or folder to a new location:
                    //System.IO.File.Move(sourceFile, destinationFile);

                    // To move a file or folder to a new location:
                    if (File.Exists(destinationFile))
                    {
                        File.Delete(destinationFile);
                    }
                    File.Move(sourceFile, destinationFile);
                }
            }
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            try { 
                var info = listView1.HitTest(e.X, e.Y);
                var row = info.Item.Index;
                var col = info.Item.SubItems.IndexOf(info.SubItem);
                var value = info.Item.SubItems[col].Text;
                //MessageBox.Show(string.Format("R{0}:C{1} val '{2}'", row, col, value));
                if(col == 1 && value != "") { 
                    MessageBox.Show(string.Format("{0}", value));
                }
            } catch (Exception ex)
            {
                return;
            }
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}


public static class MyGlobals
{
    public static int donViTinh = 1000;
    public static int titleHeaderRow = 17;
    public static int titleHeaderColumn = 3;
}