﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;

namespace ReadAndCheckExcelFile
{
    class Excel
    {
        string path = "";
        _Application excel = new _Excel.Application();
        Workbook wb;
        Worksheet ws;

        public Excel(string path, int Sheet)
        {
            this.path = path;
            this.wb = this.excel.Workbooks.Open(path);
            this.ws = this.wb.Worksheets[Sheet];
        }

        public string ReadCell(int i, int j)
        {
            i++;
            j++;
            if(this.ws.Cells[i,j].Value2 != null)
            {
                return this.ws.Cells[i, j].Value2;
            } else
            {
                return "NULL";
            }
        }
    }
}
